$(document).ready(function() {
    console.info('ready');
    $('#imageCreator input[name="image"]').change(function() {
        loadPreview(this, "#imageCreator .preview");

    })
    $('#imageEditor input[name="image"]').change(function() {
        loadPreview(this, "#imageEditor .preview");

    })


    $('#imageCreator .save').click(function(e) {
        e.preventDefault();
        submitCreateForm();
    })

    $('#imageEditor .save').click(function(e) {
        e.preventDefault();
        submitEditForm();
    })

    loadTable();

})


function loadPreview(input, target) {
    console.info('Loading preview');
    if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      $(target).attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
  }
}

function submitCreateForm() {
    var formData = new FormData(document.forms.create_form);

    $.ajax({
        type:'POST',
        url: '/images/create',
        data:formData,
        cache:false,
        contentType: false,
        processData: false,
        success:function(data){
            console.log("success");
            console.log(data);
        },
        error: function(data){
            console.log("error");
            console.log(data);
        }
    });
}

function submitEditForm() {

}


function loadTable() {
    $.ajax({
        type:'GET',
        url: '/images/all',
        cache:false,
        success:function(response){
            console.log("success");
            console.log(response);

            if (response.success) {
                response.data.records.forEach(function(item, index) {

                    $tdTitle = $('<td>').html(item.title);
                    $tdThumbnail = $('<td>').html('<img class="thumb" style="height: 32px" src="' + item.path + '"/>');
                    $tdFilename = $('<td>');
                    $tdCreatedOn = $('<td>');
                    $deleteButton = $('<button>').html('Delete').click(function() {deleteImage(item)});
                    $editButton = $('<button>').html('Edit').click(function() {editImage(item)});
                    $tdActions = $('<td>').append($deleteButton).append($editButton);

                    $('#contentBody').append(
                        $('<tr>')
                            .append($tdTitle)
                            .append($tdThumbnail)
                            .append($tdFilename)
                            .append($tdCreatedOn)
                            .append($tdActions)
                    );
                });
            }
         },
        error: function(data){
            console.log("error");
            console.log(data);
        }
    });
}

function deleteImage() {

}


function editImage(imageInfo) {
    console.info(imageInfo);
    $('#imageEditor input[name="title"]').val(imageInfo.title);
    $('#imageEditor img').attr('src', imageInfo.path);
}

function addImage() {

}
