# Test Project - Image Uploader


## How to install

- Create a new virtual host on your server.
- Clone this repository to the webroot of this new virtual host.	
- Create a new mysql database named `module_p`
- Import the DB schema located at `db/module_p.sql` into this new database.
- Open the file `modules/database/config/database.php`. Modify the connection string, username and password with your own values.
- Make sure your server is configured to allow url rewrites so that the .htaccess file works.

NOTE: The app is designed to work from the document root. If you put it inside a subfolder it might not work.

## Access the application

- The application can be accessed from your browser by accessing the following url: `http://localhost/images`. You may need to modify the hostname based on your virtual host settings.

## Usage

- The application is only partially done. The back-end is almost complete. But the front-end is only in its initial stages.
- You will see two forms. The top one is for uploading a new image and the bottom one is for editing. Only the form to create a new image works for now. Both of them are intended to be displayed inside a pop-up model later.
- Below them you'll find a table which lists existing images. This is loaded using Ajax. (However you will need to refresh the page after a new image has been uploaded, as I have not gotten that far. The code is there but I have not made a call to the function to load and redraw the table.



