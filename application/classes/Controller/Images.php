<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Images extends Controller {




	public function action_index()
	{
		$this->response->body(View::factory('index'));

	}

	public function action_create()
	{
		$this->response->headers('Content-Type', 'application/json; charset=utf-8');
		if($_SERVER['REQUEST_METHOD']=='POST') {

			try {
				$file = Validation::factory( $_FILES );
				$file->rule( 'image', array( 'Upload', 'not_empty' ) );
				$file->rule( 'image', array( 'Upload', 'valid' ) );
				if ( $file->check() ) {
					// the request is valid, do your processing

					// save the uploaded file with the name 'form' to our destination

					$filename = Upload::save( $file['image'] );
					if (! $filename) {
						throw new Exception( 'Unable to save uploaded file!' );
					}
					$filename = basename($filename);
				} else {
					throw new Exception( 'Not a valid file' );
				}


	            $newImage = new Model_Image();

				$modelFields = array(
					'title' => $this->request->post('title'),
					'path' => "upload/" . $filename,
					'created_at' => date("Y-m-d H:i:s")
				);

				$newImage->values($modelFields);



				$newImage->save();
				$imageData = array(
					'id' => $newImage->id,
					'title' => $newImage->title,
					'created_at' => $newImage->created_at,
					'path' => $newImage->path
				);
				return $this->response->body(
					json_encode(
						array(
							'success'=>true,
							'msg' => '',
							'data' => $imageData
						)
					)
				);
			}

			catch (Exception $e) {
				return $this->response->body(
					json_encode(
						array(
							'success'=>false,
							'msg' => $e->getMessage(),
							'data' => null
						)
					)
				);
			}
		}
	}

	public function action_all()
	{
		$this->response->headers('Content-Type', 'application/json; charset=utf-8');
		try {
			$ormImage = ORM::factory('Image');
			$images = $ormImage->find_all();
		} catch (Exception $e) {
			return $this->response->body(
				json_encode(
					array(
						'success'=>false,
						'msg' => $e->getMessage(),
						'data' => null
					)
				)
			);
		}

		$allImages = [];
		foreach ($images as $image) {
			$allImages[] = [
				'id'	=>	$image->id,
				'title' =>	$image->title,
				'path'	=>	$image->path
			];
		}

		return $this->response->body(
			json_encode(
				array(
					'success'=>true,
					'msg' => '',
					'data' => array(
						'total' => count($allImages),
						'records' => $allImages
					)
				)
			)
		);
	}

	public function action_update()
	{
		$this->response->headers('Content-Type', 'application/json; charset=utf-8');
		$id = $this->request->param('id');

		try {
			$ormImage = ORM::factory('Image');
			$image = $ormImage->where('id', '=', $id)->find();

			if (! $image) {
				throw new Exception("Resource not found");
			}
		} catch (Exception $e) {
			return $this->response->body(
				json_encode(
					array(
						'success'=>false,
						'msg' => $e->getMessage(),
						'data' => null
					)
				)
			);
		}

		$filename = '';
		try {
			$file = Validation::factory( $_FILES );
			$file->rule( 'image', array( 'Upload', 'not_empty' ) );
			$file->rule( 'image', array( 'Upload', 'valid' ) );
			if ( $file->check() ) {
				// the request is valid, do your processing

				// save the uploaded file with the name 'form' to our destination

				$filename = Upload::save( $file['image'] );
				if (! $filename) {
					throw new Exception( 'Unable to save uploaded file!' );
				}
				$filename = basename($filename);
			}
		} catch (Exception $e) {
			return $this->response->body(
				json_encode(
					array(
						'success'=>false,
						'msg' => $e->getMessage(),
						'data' => null
					)
				)
			);
		}

		$fillData = [];
		if (trim($this->request->post('title')) != '') {
			$fillData['title'] = trim($this->request->post('title'));
		}

		if ($filename != '') {
			$fillData['path'] = "upload/" . $filename;
			$filename = $image->path;
		}

		$image->values($fillData);

		try {
			$image->save();
			//delete the previous file.
			if ($filename != '') {
				unlink(DOCROOT . $filename);
			}
		}
		catch (Exception $e) {
			return $this->response->body(
				json_encode(
					array(
						'success'=>false,
						'msg' => $e->getMessage(),
						'data' => null
					)
				)
			);
		}

		return $this->response->body(
			json_encode(
				array(
					'success'=>true,
					'msg' => '',
					'data' => $image
				)
			)
		);

	}

	public function action_delete()
	{
		$this->response->headers('Content-Type', 'application/json; charset=utf-8');
		$id = $this->request->param('id');

		try {
			$ormImage = ORM::factory('Image');
			$image = $ormImage->where('id', '=', $id)->find();

			if (! $image) {
				throw new Exception("Resource not found");
			}
		} catch (Exception $e) {
			return $this->response->body(
				json_encode(
					array(
						'success'=>false,
						'msg' => $e->getMessage(),
						'data' => null
					)
				)
			);
		}

		try {
			unlink(DOCROOT. $image->path);
			$image->delete();

		} catch (Exception $e) {
			return $this->response->body(
				json_encode(
					array(
						'success'=>false,
						'msg' => $e->getMessage(),
						'data' => null
					)
				)
			);
		}

		return $this->response->body(
			json_encode(
				array(
					'success'=>true,
					'msg' => '',
					'data' => null
				)
			)
		);
	}

}
