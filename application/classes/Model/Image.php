<?php
class Model_Image extends ORM {

	protected $_table_columns = array(
		'id' => array(),
		'title' => array(),
		'path' => array(),
		'created_at' => array(),
		'updated_at' => array()
	);

        //Define all validations our model must pass before being saved
        //Notice how the errors defined here correspond to the errors defined in our Messages file
	public function rules() {
		return array(
			'title' => array(array('not_empty')), //Standard, build into Kohana validation library
			'path' => array(array('not_empty')),
			'created_at' => array(array('not_empty'))
		);

	}


}
