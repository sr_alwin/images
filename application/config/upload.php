<?php defined('SYSPATH') OR die('No direct access allowed.');

return array
(
    'directory' => DOCROOT.'upload',
    'create_directories' => false,
    'remove_spaces' => false
);
