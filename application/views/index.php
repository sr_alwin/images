<html>
<head>
	<title>Images</title>

</head>
<body>


	<div id="imageCreator">
		<div style="width: 400px; border:2px solid">
			<div class="title" style="width:100%; height: 40px;">
				Upload image.
				<button class="close">Close</button>
			</div>

			<div class="content" style="width:100%">
				<form name="create_form" enctype="multipart/form-data">
					<input name="title" type="text" placeholder="Title" />
					<input name="image" type="file" />
				</form name="create_form">
				<img class="preview" src="" style="height: 128px" />
			</div>

			<div class="footer" style="width:100%;">
				<button class="save">Save</button>
				<button class="cancel">Cancel</button>
			</div>
		</div>
	</div>

	<div id="imageEditor">
		<div style="width: 400px; border:2px solid">
			<div class="title" style="width:100%; height: 40px;">
				Upload image.
				<button class="close">Close</button>
			</div>

			<div class="content" style="width:100%">
				<form name="edit_form" enctype="multipart/form-data">
					<input name="title" type="text" placeholder="Title" />
					<input name="image" type="file" />
				</form name="create_form">
				<img class="preview" src="" style="height: 128px" />
			</div>

			<div class="footer" style="width:100%;">
				<button class="save">Save</button>
				<button class="cancel">Cancel</button>
			</div>
		</div>
	</div>

	<div>
		<table>
			<thead>
				<tr>
					<th>Title</th>
					<th>Thumbnail</th>
					<th>Filename</th>
					<th>Date created</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody id="contentBody">
			</tbody>
		</table>
	</div>

	<script
	src="https://code.jquery.com/jquery-2.2.4.min.js"
	integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
	crossorigin="anonymous"></script>
	<?php echo HTML::script('scripts/app.js');?>
	<script>
	console.info('ehy');
	</script>
</body>
</html>
